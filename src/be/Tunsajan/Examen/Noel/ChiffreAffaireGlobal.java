/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.Tunsajan.Examen.Noel;

/**
 *
 * @author E140928
 */
public class ChiffreAffaireGlobal {
    private float montantChiffreAffaire;
    private float mostChiffreAffaire;
    private  String bestVendeur;

    public ChiffreAffaireGlobal() {
        this.montantChiffreAffaire = 0.00f;
        this.mostChiffreAffaire = 0.00f;
        this.bestVendeur = null;
    }

    
    
    public void setMontantChiffreAffaire(float montantChiffreAffaire) {
        this.montantChiffreAffaire = montantChiffreAffaire;
    }

    public void setMostChiffreAffaire(float mostChiffreAffaire) {
        this.mostChiffreAffaire = mostChiffreAffaire;
    }

    public void setBestVendeur(String bestVendeur) {
        this.bestVendeur = bestVendeur;
    }

    
    public float getMontantChiffreAffaire() {
        return montantChiffreAffaire;
    }

    public float getMostChiffreAffaire() {
        return mostChiffreAffaire;
    }

    public String getBestVendeur() {
        return bestVendeur;
    }
    public void ajouterChiffreAffaires(float chiffreAffaire, String vendeur){
        this.setMontantChiffreAffaire(this.getMontantChiffreAffaire()+chiffreAffaire);
        if(this.getMostChiffreAffaire()<chiffreAffaire){
            this.setMostChiffreAffaire(chiffreAffaire);
            this.setBestVendeur(vendeur);
        }
        
    }
    
}
