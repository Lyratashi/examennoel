/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.Tunsajan.Examen.Noel;

/**
 *
 * @author E140928
 */
public class Menu {

    private String[] option;
    private final String TITLE;
    private int nbOptionAafficher;
    private int selection;
    private boolean sousMenu;

    public Menu(String title, String[] option, boolean sousMenu) {

        this.TITLE = title;
        this.setOption(option);
        this.setNbOptionAafficher(this.getNbOption());
        this.setSelection(-1);
        this.sousMenu = sousMenu;
    }

    /**
     * initialise les options du menu
     *
     * @param option
     */
    private void setOption(String[] option) {
        this.option = new String[option.length];
        this.option = option;
    }

    /**
     * Permet a l user de selectioné une option du menu celui-ci sera verifié
     * avant d etre valide
     */
    public void userSetSelection() {
        boolean flag = true;
        int choix = 0;

        do {
            System.out.print("-> ");
            try {
                choix = Integer.parseInt(nhpack.Console.readLine());
                if (choix <= 0 || choix > this.nbOptionAafficher + 1) { // + option Quitter ou retour
                    System.out.print("Choix incorrect !\n ");
                } else {
                    flag = false;
                }
            } catch (NumberFormatException e) {
                System.out.print("is not a number ! (" + e.getMessage() + ")\n-> ");
            }
        } while (flag);
        this.setSelection(choix);
    }

    /**
     * retourne le choix fait par l'utilisateur
     *
     * @return
     */
    final public int getSelection() {
        return this.selection;
    }

    /**
     * permet de set une selection verifiee
     *
     * @param selection
     */
    private void setSelection(int selection) {
        /* Si l user choisit de quitter et qu il n y a pas de sous menu */
        if (selection == this.nbOptionAafficher + 1 && !this.sousMenu) {
            System.exit(0);
        }
        this.selection = selection;
    }

    /**
     * Affiche les differentes options du menu
     *
     * @return
     */
    @Override
    public String toString() {
        //this.setNbOptionAafficher(nbOption);
        String outputStream;
        System.out.println();
        outputStream = "     MENU PRINCIPAL      \n" + this.TITLE + "\n";
        for (int i = 0; i < this.nbOptionAafficher; i++) {
            outputStream += "(" + (i + 1) + ")" + this.option[i]+"\n";
        }
        outputStream += "(" + (this.nbOptionAafficher + 1) + ")";
        if (this.sousMenu) {
            outputStream += " Retour\n";
        } else {
            outputStream += " Quitter\n";
        }
        return outputStream;
    }

    /**
     * Permet de gerer si des options sont disponibles ou pas
     *
     * @param szOption
     */
    final public void setNbOptionAafficher(int szOption) {
        if (szOption > this.getNbOption()) {
            return;
        }
        this.nbOptionAafficher = szOption;
    }
    public int getNbOptionAafficher(){
        return this.nbOptionAafficher;
    }

    /**
     * affiche l option selectioner par l user
     */
    public void afficherOptionSelected() {
        try {
            if (this.sousMenu && this.getSelection() == this.getNbOption() + 1) {
                System.out.println("Retour");
                return;
            }
            System.out.println(this.option[this.getSelection() - 1]);
        } catch (Exception e) {
            System.err.println("ERREUR");
        }
    }

    final public int getNbOption() {
        return this.option.length;
    }

}
