/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.Tunsajan.Examen.Noel;

/**
 *
 * @author E140928
 */
public class Programme {

    /**
     */
    public final static int NB_TRIMESTRE = 4;
    public final static float BASE_PRIME = 100000.0f;

    public static void main(String[] args) {
        Programme.run();
        // TODO code application logic here
    }

    public static void run() {
       

        String listeVendeurs[] = null;
        float[][] chiffreAffaire = null;
        ChiffreAffaireGlobal chiffreAffaireGloabal = null;

        Menu menuPrincipal = new Menu("Gestionnaire prime fin d'année", new String[]{"Demarrer Calcul des primes d'une année",
            "Enregister les chiffres d'affaires trimestriels réalisés par un vendeur",
            "Afficher les chiffres d'affaires par vendeur et les totaux", "Afficher les primes des vendeurs et clore une année"}, false);

        /* Config du menu a l ouverture du programme on affiche une seule option */
        menuPrincipal.setNbOptionAafficher(1);

        do {

            System.out.print(menuPrincipal);
            menuPrincipal.userSetSelection();
            switch (menuPrincipal.getSelection()) {
                case 1:
                    if (menuPrincipal.getNbOptionAafficher() != 1) {
                        Menu option1 = new Menu("Etes vous sur de vouloir effacer les données ?", new String[]{"Oui", "Non"}, true);
                        System.out.println(option1);
                        option1.userSetSelection();
                        if (option1.getSelection() == 2) {
                            break; /* On ne change rien */
                        }
                    }
                    listeVendeurs = UserInterface.encoderListeVendeurs(UserInterface.nbVendeurs());
                    chiffreAffaire = Statistiques.initTableauChiffreAffaire(listeVendeurs);
                    chiffreAffaireGloabal = new ChiffreAffaireGlobal();
                    menuPrincipal.setNbOptionAafficher(2);
                    break;
                case 2:
                    UserInterface.afficherListeVendeur(listeVendeurs);
                    int vendeurCurrent = UserInterface.choisirVendeur(listeVendeurs);
                    if (chiffreAffaire[0][vendeurCurrent] != 0) {
                        System.err.println("Déja encodé !");
                        break;
                    }
                    if (chiffreAffaireGloabal != null) {
                        chiffreAffaireGloabal.ajouterChiffreAffaires(UserInterface.encoderChiffreAffaireVendeur(chiffreAffaire, vendeurCurrent), listeVendeurs[vendeurCurrent]);
                        menuPrincipal.setNbOptionAafficher(menuPrincipal.getNbOption());
                    }
                    break;
                case 3:
                    UserInterface.afficherTabAffaireVendeur(chiffreAffaire, listeVendeurs, chiffreAffaireGloabal);
                    break;
                case 4:
                    UserInterface.afficherTabAffaireVendeurPrime(chiffreAffaire, listeVendeurs);
                    break;
            }

        } while (true);

    }
}
