/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.Tunsajan.Examen.Noel;

/**
 *
 * @author E140928
 */
public class Statistiques {

    public static float[] totaliserColonnes(float[][] matrice) {
        float total[] = new float[matrice[0].length];
        float buffer[] = new float[Programme.NB_TRIMESTRE];

       for(int i= 0; i<total.length;i++){
           for(int j=0; j<buffer.length;j++){
               buffer[j]=matrice[j][i];
           }
            total[i]=totaliserElements(buffer);
        }
        return total;
    }

    public static float totaliserElements(float[] tableau) {
        int somme = 0;
        for (int i = 0; i < tableau.length; i++) {
            somme += tableau[i];
        }
        return somme;
    }

    public static float[][] initTableauChiffreAffaire(String[] listeVendeurs) {
        float[][] tableau = new float[Programme.NB_TRIMESTRE][listeVendeurs.length];
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < tableau[i].length; j++) {
                tableau[i][j] = 0.0f;
            }
        }
        return tableau;
    }
}


