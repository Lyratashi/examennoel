/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.Tunsajan.Examen.Noel;

/**
 *
 * @author E140928
 */
import java.util.regex.*;

/**
 *
 * @author E140928
 */
public class UserInterface {

    public static int nbVendeurs() {
        int nbVendeurs = -1;
        boolean flag = false;
        do {
            try {
                nbVendeurs = Integer.parseInt(nhpack.Console.readLine("Combien de vendeurs à encoder ? : "));
                if (nbVendeurs <= 0) {
                    System.err.println("Doit etre strictement positif !");
                } else {
                    flag = true;
                }
            } catch (NumberFormatException e) {
                System.err.println("is not a number ! (" + e.getMessage() + ")"); /* Si pas un int */

            }
        } while (!flag);
        return nbVendeurs;
    }

    public static String encoderVendeurs(String[] szVendeurs) {

        boolean flag = true;
        String element;
        do {
            element = nhpack.Console.readLine();

            if (!Pattern.compile("[a-zA-Z 0-9]*").matcher(element).matches() || element.charAt(0) == ' ') {
                System.err.println("Erreur format");
            } else if (searchVendeurs(szVendeurs, element) != -1) {
                System.err.println("Vendeur dejà encodé !");
            } else {
                flag = false;
            }
        } while (flag);
        return element;
    }

    public static String[] encoderListeVendeurs(int nbVendeurs) {
        String listeVendeurs[] = new String[nbVendeurs];

        for (int i = 0; i < listeVendeurs.length; i++) {
            System.out.print("Vendeur " + (i + 1) + ": ");
            listeVendeurs[i] = encoderVendeurs(listeVendeurs);
        }
        return listeVendeurs;
    }
    /* Recherche une equipe dans la liste des participants
     * @param String[] list contient les equipes
     *        String element element à rechercher
     * @return int l indice de l element si touvé
     *             -1 si non trouvé
     */

    public static int searchVendeurs(String[] list, String element) {
        int indice = 0;
        while (indice < list.length && list[indice] != null) { /* Si la liste est pas remplie */

            if (list[indice].compareToIgnoreCase(element) == 0) {
                return indice;
            }
            indice++;
        }
        return -1;
    }

    public static void afficherListeVendeur(String[] listeVendeurs){
        for(String vendeur: listeVendeurs) System.out.println("- "+ vendeur);
    }

    public static float encoderChiffreAffaireVendeur(float tabChiffreAffaire[][], int vendeur) {
        float chiffreVendeur=0;
        for (int i = 0; i < tabChiffreAffaire.length; i++) {
            System.out.print("Trimestre " + (i + 1) + ": ");
           tabChiffreAffaire[i][vendeur] = encoderChiffre();
           chiffreVendeur+=tabChiffreAffaire[i][vendeur];
        }
        return chiffreVendeur;
    }

    public static float encoderChiffre() {
        boolean flag = true;
        float chiffre = 0;

        do {
            try {
                chiffre = Float.parseFloat(nhpack.Console.readLine());
                if (chiffre < 0) {
                    System.out.print("ERREUR ne peut être negatif !\n-> ");
                } else {
                    flag = false;
                }
            } catch (NumberFormatException e) {
                System.out.print("is not a number ! (" + e.getMessage() + ")\n-> ");
            }
        } while (flag);
        return chiffre;
    }


    public static int choisirVendeur(String[] listeVendeur) {
        boolean flag;
        int choix = 0;
        do {
            flag = false;
            choix = searchVendeurs(listeVendeur, nhpack.Console.readLine("Recherche: "));
            System.out.println();
            if (choix == -1) {
                System.err.println("vendeur non trouvée !");
                flag = true;
            }
        } while (flag);

        return choix;
    }
    //Methode Affichage
    public static void afficherTabAffaireVendeur(float tableau[][], String listeVendeur[], ChiffreAffaireGlobal chiffreAffaireGloabal ) {
        float total[] = Statistiques.totaliserColonnes(tableau);
        System.out.println("CH.D'AFFAIRE\tT1\tT2\tT3\tT4\tTOTAL");
        int cptVendeur = 0;
        for (int i = 0; i < tableau[0].length; i++) {
            System.out.print(listeVendeur[i]+"\t");
            for(int j=0; j<tableau.length; j++){
                System.out.print(tableau[j][i]+"\t\t");
            }
            System.out.print(total[i]+"\t\t\n"); 
        }
        System.out.println("\n Le chiffre d'affaire total est de: " + chiffreAffaireGloabal.getMontantChiffreAffaire());
        
        System.out.println("Plus grand chiffre affaire est de " + chiffreAffaireGloabal.getMostChiffreAffaire() + " realise par " + chiffreAffaireGloabal.getBestVendeur());

    }
    public static void afficherTabAffaireVendeurPrime(float tableau[][], String listeVendeur[]) {
        float total[]=new float[listeVendeur.length];
        System.out.println("CH.D'AFFAIRE\tT1\tT2\tT3\tT4\tTOTAL");
        for (int i = 0; i < tableau[0].length; i++) {
            System.out.print(listeVendeur[i]+"\t");
            for(int j=0; j<tableau.length; j++){
                if(tableau[j][i]>Programme.BASE_PRIME){
                    float prime=((tableau[j][i]-Programme.BASE_PRIME)/100)*10;
                    System.out.print(prime+"\t");
                    total[i]+=prime;
                }
                else System.out.print("0.0");
            }
            System.out.print(total[i]+"\t\t\n");
        }
        System.out.println("Total des prime "+ Statistiques.totaliserElements(total));

    }
}
